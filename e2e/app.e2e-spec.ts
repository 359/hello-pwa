import { HelloPwaPage } from './app.po';

describe('hello-pwa App', function() {
  let page: HelloPwaPage;

  beforeEach(() => {
    page = new HelloPwaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
